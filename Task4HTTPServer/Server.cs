using System.Net;
using System.Net.Sockets;

namespace Task4HTTPServer;

public class Server
{
    private readonly TcpListener _tcpListener;

    public Server(int port)
    {
        _tcpListener = new TcpListener(IPAddress.Any, port);
        _tcpListener.Start();

        while (true)
        {
            new Client(_tcpListener.AcceptTcpClient());
        }
    }

    ~Server()
    {
        if (_tcpListener != null)
        {
            _tcpListener.Stop();
        }
    }
}
