using System.Net.Sockets;
using System.Text;

namespace Task4HTTPServer;

public class Client
{
    public Client(TcpClient client)
    {
        string message = "HTTP/1.1 200 OK\r\n" +
                         // "Content-Length: 21\r\n" +
                         "Content-Length: 100\r\n" +
                         "Content-type: text/html; charset=utf-8\r\n" +
                         "Connection: close\r\n" +
                         "\r\n" +
                         "<h1>Hello world!</h1>"; 
        byte[] buffer = Encoding.UTF8.GetBytes(message);
        client.GetStream().Write(buffer, 0, buffer.Length);
        while (true)
        {        
            string letter = Console.ReadKey().KeyChar.ToString();
            byte[] buffer2 = Encoding.UTF8.GetBytes(letter);
            client.GetStream().Write(buffer2, 0, buffer2.Length);
        }
        
        client.Close();
    }
}
