Консольное приложение, которое открывает порт и при коннекте на него пишет в него                     
<pre>
HTTP/1.1 200 OK\r\n
Content-Length: 21\r\n
content-type: text/html; charset=utf-8\r\n
Connection: close\r\n
\r\n
&lt;h1>Hello world!&lt;/h1>
</pre>

Открыть в браузере localhost с этим портом и убедиться, что появилась большая надпись.
Поменять Content-Length: 21 на Content-Length: 10 и еще раз запустить в браузере.
Дополнительно: убрать Content-Length: 21, не закрывать конект и отправлять в него после Hello World каждое нажатие кнопки.
